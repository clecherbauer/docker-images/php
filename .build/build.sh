#!/bin/bash

set -e

export LC_ALL=C
export DEBIAN_FRONTEND=noninteractive
aptInstall='apt-get install -y --no-install-recommends'

# add repo for stretch
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'

apt-get update

# webp support for imagemagick: https://github.com/rosell-dk/webp-convert/wiki/Installing-Imagick-extension-with-WebP-support
$aptInstall gcc \
            make \
            libjpeg-dev \
            libpng-dev \
            libtiff-dev \
            libgif-dev \
            webp

wget https://storage.googleapis.com/downloads.webmproject.org/releases/webp/libwebp-1.1.0.tar.gz
tar xvzf libwebp-1.1.0.tar.gz && cd libwebp-1.1.0 && ./configure --disable-dependency-tracking && make install && cd .. && rm -rf libwebp-1.1.0
apt remove gcc make -y
apt autoremove -y

# install apache
$aptInstall apache2 libapache2-mod-xsendfile

$aptInstall imagemagick \
            libapache2-mod-php7.4 \
            libssh2-1 \
            php7.4

$aptInstall php7.4-apcu \
            php7.4-apcu-bc \
            php7.4-bcmath \
            php7.4-cli \
            php7.4-common \
            php7.4-curl \
            php7.4-gd \
            php7.4-mysqlnd \
            php7.4-memcache \
            php7.4-memcached \
            php7.4-imagick \
            php7.4-imap \
            php7.4-intl \
            php7.4-mbstring \
            php-pear \
            php7.4-soap \
            php7.4-sqlite \
            php7.4-ssh2 \
            php7.4-xdebug \
            php7.4-xml \
            php7.4-xsl \
            php7.4-zip

apt-get clean
rm -rf /var/lib/apt/lists/*
rm -rf /tmp/*

# install runit apache service
/.build/services/apache2/apache2.sh

# enable php modules
phpenmod bcmath \
         curl \
         gd \
         memcache \
         memcached \
         mysqlnd \
         imagick \
         imap \
         intl \
         xdebug

# enable apache modules
a2enmod php7.4 rewrite headers xsendfile expires

echo "ServerTokens Prod" >> /etc/apache2/apache2.conf
echo "ServerSignature Off" >> /etc/apache2/apache2.conf

# server php settings
echo "xdebug.max_nesting_level=1000" >> /etc/php/7.4/custom.ini
echo "date.timezone=Europe/Zurich" >> /etc/php/7.4/custom.ini
ln -s /etc/php/7.4/custom.ini /etc/php/7.4/apache2/conf.d/custom.ini
ln -s /etc/php/7.4/custom.ini /etc/php/7.4/cli/conf.d/custom.ini

# install composer
curl https://getcomposer.org/composer-stable.phar > /usr/bin/composer
chmod +x /usr/bin/composer
