FROM registry.gitlab.com/clecherbauer/docker-images/debian:buster

USER root

COPY .build /.build
RUN /.build/build.sh

EXPOSE 80
